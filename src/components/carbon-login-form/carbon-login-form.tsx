import { Component } from "@stencil/core";

@Component({
  tag: 'carbon-login-form',
  styleUrl: 'carbon-login-form.scss'
})
export class CarbonLoginForm {

  render() {
    return (
      <p>Carbon Login Form</p>
    );
  }
}
